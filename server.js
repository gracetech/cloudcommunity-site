// server.js

// set up ======================================================================
// get all the tools we need
var express  = require('express');
var app      = express();
var port     = process.env.PORT || 9000;
var passport = require('passport');
var flash    = require('connect-flash');
var path = require('path');
var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var favicon = require('express-favicon');
var compression = require('compression');
var MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    CollectionDriver = require('./app/collectionDriver').CollectionDriver;

var busboy = require('connect-busboy');


require('./config/passport')(passport); // pass passport for configuration

var mongoHost = 'localHost';
var mongoPort = 27017;
var collectionDriver;

var mongoClient = new MongoClient(new Server(mongoHost, mongoPort));
mongoClient.open(function(err, mongoClient) {
    if (!mongoClient) {
        console.error("Error! Exiting... Must start MongoDB first");
        process.exit(1);
    }
    var db = mongoClient.db("cloudcommunity");
    collectionDriver = new CollectionDriver(db);


// set up our express application
    app.use(compression({ threshold: 512 }));
    app.use(morgan('dev')); // log every request to the console
    app.use(cookieParser()); // read cookies (needed for auth)
    app.use(bodyParser.json()); // get information from html forms
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(busboy());

    app.use(favicon(__dirname + '/views/assets/images/favicon.ico'));

    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'ejs'); // set up jade for templating

// required for passport
    app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
    app.use(passport.initialize());
    app.use(passport.session()); // persistent login sessions
    app.use(flash()); // use connect-flash for flash messages stored in session
    var oneDay = 86400000;
    app.use(express.static(path.join(__dirname, '/views/assets/'), { maxAge: oneDay }));

// routes ======================================================================
    require('./app/routes.js')(app, passport, collectionDriver); // load our routes and pass in our app and fully configured passport

// launch ======================================================================
    app.listen(port);

    console.log('The magic happens on port ' + port);
});
