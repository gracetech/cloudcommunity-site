// load all the things we need
var LocalStrategy    = require('passport-local').Strategy;

// load up the user model
var User       = require('../app/models/user');
var crypto = require('crypto');

module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.username);
    });

    // used to deserialize the user
    passport.deserializeUser(function(name, done) {
        User.findOne(name, function(user) {

            done(null, user);
        });
    });

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, email, password, done) {
        console.log("login ok :", email, password);
        // asynchronous
        process.nextTick(function() {
            User.findOne(email, function(user) {
                // if no user is found, return the message
                if (!user)
                    return done(null, false, req.flash('loginMessage', 'No user found.'));
                var md5 = crypto.createHash('md5');
                if (!md5.update(password).digest('hex').toLowerCase() == user.password) {
                    return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
                }else
                    return done(null, user);
            });
        });

    }));
};
