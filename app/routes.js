module.exports = function(app, passport, collectionDriver) {

	var FS = require('fs');
	var ObjectID = require('mongodb').ObjectID;

// normal routes ===============================================================

	// show the home page (will also have our login links)
	app.get('/', function(req, res) {
		res.render('index');
	});

	// LOGOUT ==============================
	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/index');
	});

// =============================================================================
// AUTHENTICATE (FIRST LOGIN) ==================================================
// =============================================================================

	// locally --------------------------------
	// LOGIN ===============================
	// show the login form
	app.get('/login', function(req, res) {
		res.render('login', { message: req.flash('loginMessage') });
	});

	// process the login form
	app.post('/login', passport.authenticate('local-login', {
		successRedirect : '/util', // redirect to the secure profile section
		failureRedirect : '/login', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	var dbnames = ["account", "checkin", "configcenter", "device", "feedback", "mobilecharge", "notification", "order", "quote", "track", "store_regcode"];
	var databaseHeaders = {
		account : 		["_id", "username", "password", "bonus", "avatar_id", "created_at"],
		checkin : 		["_id", "userid", "date", "created_at"],
		configcenter : 	["_id", "bigVersion", "version", "platform", "config", "created_at"],
		device : 		["_id", "deviceID", "os", "platform", "version", "created_at"],
		feedback : 		["_id", "content", "user", "os", "platform", "version", "created_at"],
		track : 		["_id", "uuid", "detail", "created_at"],
		store_regcode : ["_id", "storeID", "regcode", "dateID", "created_at"]
	};

	app.post('/httppost', isLoggedIn, function(req, res) {

		console.log('httppost', req.body);

		req.pipe(req.busboy);
		var info = {};
		req.busboy.on('field', function (fieldname, val, valTruncated, keyTruncated) {
			info[fieldname] = val;
			console.log(info);
		});
		req.busboy.on('finish', function () {

			console.log(info);

			next();
		});
	});

	app.post('/dbcommand', isLoggedIn, function(req, res) {

		console.log('dbcomand', req.body);

		var callback = function(err, docs){
			var result = docs;
			if (err) { result = err; }
			try {

				result = JSON.stringify(result, undefined, 4);

			}catch (e){

			}

			console.log('result: ' + result);

			// pass data into template and render
			var data = {
				active : 'util',
				messages : [],
				tasks : [],
				alerts : [],
				dbnames : dbnames,
				dbresult :  result.trim() //syntaxHighlight(result)
			};

			res.render('util', data);
		};

		var parser = function(obj, callback) {

			if ('string' == typeof obj) {

				try {
					obj = JSON.parse(obj);
				}catch (e){
					//just simple string
					console.log('JSON.parse Error: ', e);
				}

				if ('string' == typeof obj) {

					obj = {_id : ObjectID(obj) }
				}
			}

			return obj;
		};

		var dbname = req.body.dbname;
		var action = req.body.action;
		var arg1 = req.body.arg1;
		var arg2 = req.body.arg2;

		switch (action) {
			case 'find': {

				if (arg1 && arg1.length > 0) {

					var condition = parser(arg1);

					collectionDriver.findOne(dbname, condition, callback);
				}else{
					collectionDriver.findAll(dbname, callback);
				}
				break;
			}

			case 'update': {
				var condition = parser(arg1);
				var newObj = parser(arg2);

				collectionDriver.update(dbname, condition, newObj, function(){
					collectionDriver.findOne(dbname, condition, callback);
				});
				break;
			}
			case 'delete': {
				var cond = parser(arg1);
				collectionDriver.delete(dbname, cond, callback);
				break;
			}
			default : {
				break;
			}
		}


	});

	app.get('/index', function(req, res) {
		res.render('index');
	});

	app.get('/admin', isLoggedIn, function(req, res) {
		res.render('admin', {
			active : 'admin',
			messages : [],
			tasks : [],
			alerts : []
		});
	});

	app.get('/util', isLoggedIn, function(req, res) {
		res.render('util', {
			active : 'util',
			messages : [],
			tasks : [],
			alerts : [],
			dbnames : dbnames,
			dbresult : ""
		});
	});

	app.get('/database', isLoggedIn, function(req, res) {

		console.log(req.query);
		var dbname = req.query.db;
		var headers = databaseHeaders[dbname];

		if (!dbname) {
			dbname = '';
			res.render('database', {
				active : 'database',
				messages : [],
				tasks : [],
				alerts : [],
				dbnames : dbnames,
				databasename : dbname,
				headers : [],
				data : []
			});
		}else
		{
			if (!headers) {
				res.render('database', {
					active : 'database',
					messages : [],
					tasks : [],
					alerts : [],
					dbnames : dbnames,
					databasename : dbname,
					headers : [],
					data : []
				});
			}else {
				collectionDriver.findAll(dbname, function(error, objs) {
					if (error) { res.send(400, error); }
					else {
						res.render('database', {
							active : 'database',
							messages : [],
							tasks : [],
							alerts : [],
							dbnames : dbnames,
							databasename : dbname,
							headers : databaseHeaders[dbname],
							data : objs
						});
					}
				});
			}
		}
	});

	app.post('/upload', isLoggedIn, function(req, res) {
		var fstream;
		req.pipe(req.busboy);
		req.busboy.on('file', function (fieldname, file, filename) {
			if (filename && filename.length > 0) {
				console.log("Uploading: " + filename);
				fstream = FS.createWriteStream(__dirname + '/upload/' + filename);
				file.pipe(fstream);
				fstream.on('close', function () {
					res.send('Upload ' + filename + ' ok!');
				});
			}
		});
	});
};

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
	if (req.isAuthenticated())
		return next();

	res.redirect('/index');
}
