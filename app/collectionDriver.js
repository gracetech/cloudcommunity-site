var ObjectID = require('mongodb').ObjectID;

CollectionDriver = function(db) {
  this.db = db;
};

CollectionDriver.prototype.getCollection = function(collectionName, callback) {
  this.db.collection(collectionName, function(error, the_collection) {
    if( error ) callback(error);
    else callback(null, the_collection);
  });
};

//find all objects for a collection
CollectionDriver.prototype.findAll = function(collectionName, callback) {
    this.getCollection(collectionName, function(error, the_collection) {
      if( error ) callback(error);
      else {
        the_collection.find().toArray(function(error, results) {
          if( error ) callback(error);
          else callback(null, results)
        });
      }
    });
};

CollectionDriver.prototype.findOne = function(collectionName, arg, callback) {
    this.getCollection(collectionName, function(error, the_collection) {
      if( error ) callback(error);
      else {
		  	the_collection.findOne(arg, function(error,doc) {
            	if (error) callback(error);
            	else callback(null, doc);
            });
           }
    });
};

CollectionDriver.prototype.find = function(collectionName, arg, callback) {
    this.getCollection(collectionName, function(error, the_collection) {
        if( error ) callback(error);
        else {
            var cursor = the_collection.find(arg);
            var result = [];
            cursor.each(function(iLooper) {
                result.push(iLooper);
            });
            callback(null, result);
        }
    });
};

//find a specific object
CollectionDriver.prototype.get = function(collectionName, id, callback) {
    if (!(id instanceof ObjectID)) {
        id = ObjectID(id);
    }

	this.findOne(collectionName, {'_id':ObjectID(id)}, callback);
};

//save new object
CollectionDriver.prototype.save = function(collectionName, obj, callback) {
    this.getCollection(collectionName, function(error, the_collection) {
      if( error ) {
          if (callback) { callback(error) }
      }else {
        obj.created_at = new Date();
        the_collection.insert(obj, function(ret) {
            if (callback) { callback(null, obj) }
        });
      }
    });
};

//update object
CollectionDriver.prototype.update = function(collectionName, criteria, objNew, callback) {
    this.getCollection(collectionName, function(error, the_collection) {
        if (error) {
            if (callback) { callback(error); }
        }else {
            the_collection.update(criteria, objNew, function(error) {
                if (error) {
                    if (callback) { callback(error); }
                }else {
                    if (callback) { callback(null, objNew); }
                }
            });
        }
    });
};

//delete a specific object
CollectionDriver.prototype.delete = function(collectionName, entityId, callback) {
    this.getCollection(collectionName, function(error, the_collection) {
        if (error) callback(error);
        else {
            the_collection.remove({'_id':ObjectID(entityId)}, function(error,doc) {
            	if (error) callback(error);
            	else callback(null, doc);
            });
        }
    });
};

CollectionDriver.prototype.drop = function(collectionName, callback) {
    this.getCollection(collectionName, function(error, the_collection) {
        if (error) callback(error);
        else {
            the_collection.drop(function(error,doc) {
            	if (error) callback(error);
            	else callback(null, doc);
            });
        }
    });
};

exports.CollectionDriver = CollectionDriver;